import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig1 = new Matriz(3,2);
        Matriz orig2 = new Matriz(3,2);
        Matriz soma, prod;
        
        double[][] m1 = orig1.getMatriz();
        m1[0][0] = 1;
        m1[0][1] = 2;
        m1[1][0] = 3;
        m1[1][1] = 4;
        m1[2][0] = 5;
        m1[2][1] = 6;
        
        double[][] m2 = orig2.getMatriz();
        m2[0][0] = 9;
        m2[0][1] = 8;
        m2[1][0] = 7;
        m2[1][1] = 6;
        m2[2][0] = 5;
        m2[2][1] = 4;
        
        soma = orig1.soma(orig2);
        
        Matriz transp = orig2.getTransposta();
        prod = orig1.prod(transp);
                
        System.out.println("Matriz original 1: " + orig1);
        System.out.println("Matriz original 2: " + orig2);
        System.out.println("Matriz soma: " + soma);
        System.out.println("Matriz produto: " + prod);
    }
}
